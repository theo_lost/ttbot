import logging
import json
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler
from elo import Elo
from key import TOKEN


FILENAME = "data.json"

elo_calculator = Elo(k_factor=20)


def generate_leaderboard():
    elodict = file_to_dict()
    sorted_elodict = sorted(elodict.items(), key=lambda x: x[1], reverse=True)
    leaderboard = "Score Leaderboard:\n"
    for i, (user, elo) in enumerate(sorted_elodict, start=1):
        leaderboard += f"{i}. {user}: {round(elo)}\n"
    return leaderboard


def match(winner: str, looser: str, draw: bool = False):
    elodict = file_to_dict()
    elos_before = (elodict[winner], elodict[looser])
    elos_after = elo_calculator.rate_1vs1(elodict[winner], elodict[looser], drawn=draw)
    elodict[winner], elodict[looser] = elos_after
    dict_to_file(elodict)

    gain = elos_after[0] - elos_before[0]
    message = f"@{winner} won against @{looser}  - Score: +{round(gain, 2)}\n{winner}: {round(elos_before[0])} -> {round(elos_after[0])}\n{looser}: {round(elos_before[1])} -> {round(elos_after[1])}"
    return message


def dict_to_file(dic: dict):
    with open(FILENAME, "w") as outfile:
        json.dump(dic, outfile)

def file_to_dict():
    with open(FILENAME) as json_file:
        return json.load(json_file)


async def answer(message: str, update: Update, context: ContextTypes.DEFAULT_TYPE, general: bool = False):
    if not general:
        username = update.effective_user.username
        await context.bot.send_message(chat_id=update.effective_chat.id, text = "@" + username + " " + message)
    else:
        await context.bot.send_message(chat_id=update.effective_chat.id, text = message)

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)



async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    elodict = file_to_dict()
    username = update.effective_user.username
    if username in elodict:
        await answer("You are already registered with a score of " + str(round(elodict[username])), update, context)
    else:
        elodict[username] = 1000.0
        await answer("You are newly registerd and your score has been set to " + str(round(elodict[username])), update, context)
        dict_to_file(elodict)


async def elo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    elodict = file_to_dict()
    username = update.effective_user.username
    elo = elodict[username]
    await answer("Your score is " + str(round(elo)), update, context)


async def win(update: Update, context: ContextTypes.DEFAULT_TYPE):
    winner = update.effective_user.username
    looser = update.effective_message.text.split()[1][1:]
    message = match(winner, looser)
    await answer(message, update, context, True)


async def leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    message = generate_leaderboard()
    await answer(message, update, context, True)



if __name__ == '__main__':
    application = ApplicationBuilder().token(TOKEN).build()
    
    start_handler = CommandHandler('start', start)
    elo_handler = CommandHandler('score', elo)
    win_handler = CommandHandler( 'win', win)
    leaderboard_handler = CommandHandler('leaderboard', leaderboard)

    application.add_handler(start_handler)
    application.add_handler(elo_handler)
    application.add_handler(win_handler)
    application.add_handler(leaderboard_handler)


    application.run_polling()